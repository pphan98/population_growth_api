# Population Growth API

Brief decription of the API
- It is hosted on https://population-growth-api.herokuapp.com/ and the endpoint url is https://population-growth-api.herokuapp.com/api/v1/locations/:zip
- Contains the single endpoint that provides 2014 and 2015 population data given a zip code.
- This endpoint returns data in this structure:

```ruby
{
    location: {
        zip_code: '90210',
        cbsa: '31080',
        msa: 'Los Angeles-Long Beach-Anaheim, CA',
        pop_2015: 13340068,
        pop_2014: 13254397
    }
}
```

- The locations table (where all the data is stored) is indexed by `zip_code` for quicker lookup
- This endpoint returns a 404 when a zip_code is not found.
- The data gets imported from the two CSVs via a rake task that parses through them and upserts into the API's database.
- The above mentioned rake task is also set to periodically run to keep the data up-to-date (similar to a cron job)
- API has good test coverage

A few notes if you want to run this API on your local machine:
- Pull this project down and do the usual rails setup
- After running `rake db:create` and `rake db:migrate`, you can run the rake task to import data: `rake locations:import`. Before running the importer, you will need to set up Sidekiq and Redis.
