module V1
  class LocationsController < ApplicationController
    def show
      render json: location, serializer: LocationSerializer
    end

    private

    def location
      Location.find_by!(zip_code: params[:zip])
    end
  end
end
