class Location < ApplicationRecord
  validates :zip_code, presence: true, uniqueness: true
end
