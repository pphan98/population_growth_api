class LocationSerializer < ActiveModel::Serializer
  attributes :zip_code,
             :cbsa,
             :msa,
             :pop_2015,
             :pop_2014
end
