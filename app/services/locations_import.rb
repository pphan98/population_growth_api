class LocationsImport
  class << self
    def perform
      new.import
    end
  end

  def import
    ::CSV.parse(zip_to_cbsa_data, headers: true, header_converters: :symbol) do |row|
      location_upsert(row)
    end
  end

  private

  def zip_to_cbsa_data
    @zip_to_cbsa_data ||= begin
      resp = Faraday.get(zip_to_cbsa_csv_url)
      fail unless resp.success?
      resp.body.force_encoding('utf-8')
    end
  end

  def zip_to_cbsa_csv_url
    ENV.fetch('ZIP_TO_CBSA_CSV_URL')
  end

  def location_upsert(zip_to_cbsa_row)
    zip_code = zip_to_cbsa_row[:zip]
    return if zip_code.blank?
    cbsa = zip_to_cbsa_row[:cbsa]
    location = Location.find_or_initialize_by(zip_code: zip_code)
    location.update_attributes(location_attributes(cbsa))
  end

  def location_attributes(cbsa)
    return {} if cbsa.blank? || cbsa == '99999'
    true_cbsa = adjusted_cbsa(cbsa)
    pop_data = parsed_cbsa_to_msa_data.find do |row|
      row[:cbsa] == true_cbsa && row[:lsad] == 'Metropolitan Statistical Area'
    end
    return {} if pop_data.blank?
    {
      cbsa: true_cbsa,
      msa: pop_data[:name],
      pop_2015: pop_data[:popestimate2015],
      pop_2014: pop_data[:popestimate2014]
    }
  end

  def adjusted_cbsa(cbsa)
    mdiv_row = parsed_cbsa_to_msa_data.find do |row|
      row[:mdiv] == cbsa
    end
    return cbsa if mdiv_row.blank?
    mdiv_row[:cbsa]
  end

  def parsed_cbsa_to_msa_data
    @parsed_cbsa_to_msa_data ||= ::CSV.parse(
      cbsa_to_msa_data,
      headers: true,
      header_converters: :symbol
    )
  end

  def cbsa_to_msa_data
    @cbsa_to_msa_data ||= begin
      resp = Faraday.get(cbsa_to_msa_csv_url)
      fail unless resp.success?
      resp.body
    end
  end

  def cbsa_to_msa_csv_url
    ENV.fetch('CBSA_TO_MSA_CSV_URL')
  end
end
