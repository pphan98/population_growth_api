class LocationsImportWorker
  include Sidekiq::Worker

  def perform
    LocationsImport.perform
  end
end
