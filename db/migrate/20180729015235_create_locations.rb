class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :zip_code, null: false
      t.string :cbsa
      t.string :msa
      t.bigint :pop_2015
      t.bigint :pop_2014

      t.timestamps
    end
    add_index :locations, :zip_code, unique: true
  end
end
