namespace :locations do
  desc 'Import locations from HUD & Census data'
  task import: [:environment] do
    LocationsImportWorker.perform_async
  end
end
