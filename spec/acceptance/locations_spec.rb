require 'acceptance_helper'

RSpec.resource 'Location' do
  describe 'GET - Location Show' do
    get '/api/v1/locations/:zip' do
      header 'Accept', 'application/json'
      header 'Content-Type', 'application/json'

      let(:zip) { '90210' }
      let!(:location) { create(:location, zip_code: zip) }

      example 'GET - Location Show - Success' do
        do_request
        expect(response_status).to eq(200)
        resp = JSON.parse(response_body)
        location_resp = resp['location']
        expect(location_resp.keys).to match_array(
          %w[zip_code cbsa msa pop_2015 pop_2014]
        )
        expect(location_resp['zip_code']).to eq(location.zip_code)
        expect(location_resp['cbsa']).to eq(location.cbsa)
        expect(location_resp['msa']).to eq(location.msa)
        expect(location_resp['pop_2015']).to eq(location.pop_2015)
        expect(location_resp['pop_2014']).to eq(location.pop_2014)
      end
    end
  end
end

