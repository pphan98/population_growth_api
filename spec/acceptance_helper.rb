require 'rails_helper'
require 'rspec_api_documentation'
require 'rspec_api_documentation/dsl'

RspecApiDocumentation.configure do |config|
  config.format = %i[json combined_text html]
  config.curl_host = 'http://localhost:3000'
  config.curl_headers_to_filter = %w[Cookie Host]
  config.api_name = 'Population Growth API Docs'

  # Change how the post body is formatted by default, you can still override by `raw_post`
  # Can be :json, :xml, or a proc that will be passed the params
  config.request_body_formatter = proc { |params| params.to_json }
  config.docs_dir = Rails.root.join('doc', 'api')
end
