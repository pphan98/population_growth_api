FactoryBot.define do
  factory :location do
    sequence(:zip_code) { |n| sprintf('%05d', n) }
    cbsa '88888'
    msa 'Los Angeles County, CA'
    pop_2015 888_888
    pop_2014 777_777
  end
end
