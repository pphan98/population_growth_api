require 'rails_helper'

RSpec.describe Location, type: :model do
  describe 'validations' do
    let(:location) { build(:location) }

    describe 'zip code presence validation' do
      it 'should not be valid if zip code is missing' do
        location.zip_code = nil
        expect(location.valid?).to eq(false)
      end
    end

    describe 'zip code uniqueness validation' do
      let(:location_2) { build(:location) }

      it 'should not be valid if another location already has the same zip code' do
        location.save
        location_2.zip_code = location.zip_code
        expect(location_2.valid?).to eq(false)
      end
    end
  end
end
