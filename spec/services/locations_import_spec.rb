require 'rails_helper'

RSpec.describe LocationsImport, type: :service do
  subject(:importer) { described_class.new }

  # We want to handle edge cases where there are empty rows or empty CBSAs.
  let(:zip_to_cbsa_data) do
    "ZIP,CBSA,RES_RATIO,BUS_RATIO,OTH_RATIO,TOT_RATIO\r"\
    "00501,35004,0.000000000,1.000000000,0.000000000,1.000000000\r"\
    ",,,,,\r"\
    "00601,10260,1.000000000,1.000000000,1.000000000,1.000000000\r"\
    "00602,10380,1.000000000,1.000000000,1.000000000,1.000000000\r"\
    "00603,10380,1.000000000,1.000000000,1.000000000,1.000000000\r"\
    "00604,10380,1.000000000,1.000000000,1.000000000,1.000000000\r"\
    "00605,10380,1.000000000,1.000000000,1.000000000,1.000000000\r"\
    "00606,99999,1.000000000,1.000000000,1.000000000,1.000000000\r"\
    "00610,,1.000000000,1.000000000,1.000000000,1.000000000"
  end

  # Empty rows exist in this csv, so this setup reflects that.
  let(:cbsa_to_msa_data) do
    "CBSA,MDIV,STCOU,NAME,LSAD,CENSUS2010POP,ESTIMATESBASE2010,POPESTIMATE2010,POPESTIMATE2011,POPESTIMATE2012,POPESTIMATE2013,POPESTIMATE2014,POPESTIMATE2015,NPOPCHG2010,NPOPCHG2011,NPOPCHG2012,NPOPCHG2013,NPOPCHG2014,NPOPCHG2015,BIRTHS2010,BIRTHS2011,BIRTHS2012,BIRTHS2013,BIRTHS2014,BIRTHS2015,DEATHS2010,DEATHS2011,DEATHS2012,DEATHS2013,DEATHS2014,DEATHS2015,NATURALINC2010,NATURALINC2011,NATURALINC2012,NATURALINC2013,NATURALINC2014,NATURALINC2015,INTERNATIONALMIG2010,INTERNATIONALMIG2011,INTERNATIONALMIG2012,INTERNATIONALMIG2013,INTERNATIONALMIG2014,INTERNATIONALMIG2015,DOMESTICMIG2010,DOMESTICMIG2011,DOMESTICMIG2012,DOMESTICMIG2013,DOMESTICMIG2014,DOMESTICMIG2015,NETMIG2010,NETMIG2011,NETMIG2012,NETMIG2013,NETMIG2014,NETMIG2015,RESIDUAL2010,RESIDUAL2011,RESIDUAL2012,RESIDUAL2013,RESIDUAL2014,RESIDUAL2015\r\n"\
    ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\r\n"\
    ",,,Metropolitan Statistical Area,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\r\n"\
    ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\r\n"\
    "35004,,,\"Abilene, TX\",Metropolitan Statistical Area,165252,165252,165609,166639,167578,167549,168380,169578,357,1030,939,-29,831,1198,540,2292,2359,2391,2301,2344,391,1502,1585,1691,1645,1669,149,790,774,700,656,675,89,227,513,389,384,433,119,12,-348,-1158,-203,74,208,239,165,-769,181,507,0,1,0,40,-6,16\r\n"\
    "35004,,48059,\"Callahan County, TX\",County or equivalent,13544,13544,13518,13535,13519,13528,13520,13557,-26,17,-16,9,-8,37,31,120,122,136,124,127,59,158,142,176,173,172,-28,-38,-20,-40,-49,-45,0,5,4,7,7,8,4,39,2,45,45,87,4,44,6,52,52,95,-2,11,-2,-3,-11,-13\r\n"\
    "35004,,48253,\"Jones County, TX\",County or equivalent,20202,20198,20223,20240,19858,20005,19816,19970,25,17,-382,147,-189,154,25,155,191,189,167,176,21,213,173,205,182,154,4,-58,18,-16,-15,22,3,10,11,13,15,15,21,50,-420,178,-199,149,24,60,-409,191,-184,164,-3,15,9,-28,10,-32\r\n"\
    "35004,,48441,\"Taylor County, TX\",County or equivalent,131506,131510,131868,132864,134201,134016,135044,136051,358,996,1337,-185,1028,1007,484,2017,2046,2066,2010,2041,311,1131,1270,1310,1290,1343,173,886,776,756,720,698,86,212,498,369,362,410,94,-77,70,-1381,-49,-162,180,135,568,-1012,313,248,5,-25,-7,71,-5,61\r\n"\
    "10420,10260,,\"Akron, OH\",Metropolitan Statistical Area,703200,703207,703119,703180,702674,703788,704835,704243,-88,61,-506,1114,1047,-592,1974,7578,7554,7582,7692,7651,1595,6659,7030,7016,6907,6967,379,919,524,566,785,684,259,1177,1300,1383,1593,1586,-722,-1758,-2332,-926,-804,-2738,-463,-581,-1032,457,789,-1152,-4,-277,2,91,-527,-124\r\n"\
    "10420,10260,39133,\"Portage County, OH\",County or equivalent,161419,161421,161448,161899,161482,161593,162235,162275,27,451,-417,111,642,40,411,1511,1384,1420,1508,1492,331,1290,1365,1380,1354,1343,80,221,19,40,154,149,68,326,353,379,434,431,-122,-8,-804,-339,150,-511,-54,318,-451,40,584,-80,1,-88,15,31,-96,-29\r\n"\
    "10420,10260,39153,\"Summit County, OH\",County or equivalent,541781,541786,541671,541281,541192,542195,542600,541968,-115,-390,-89,1003,405,-632,1563,6067,6170,6162,6184,6159,1264,5369,5665,5636,5553,5624,299,698,505,526,631,535,191,851,947,1004,1159,1155,-600,-1750,-1528,-587,-954,-2227,-409,-899,-581,417,205,-1072,-5,-189,-13,60,-431,-95\r\n"\
    "10380,,,\"Albany, GA\",Metropolitan Statistical Area,157308,157500,157665,157805,157370,155864,155020,153526,165,140,-435,-1506,-844,-1494,543,2336,2183,2213,2052,2085,410,1242,1395,1458,1463,1427,133,1094,788,755,589,658,18,92,129,122,129,139,14,-1131,-1372,-2338,-1528,-2259,32,-1039,-1243,-2216,-1399,-2120,0,85,20,-45,-34,-32\r\n"\
    "10380,,13007,\"Baker County, GA\",County or equivalent,3451,3451,3435,3317,3367,3324,3274,3180,-16,-118,50,-43,-50,-94,4,35,37,31,31,34,2,27,16,25,16,20,2,8,21,6,15,14,0,3,5,5,6,6,-18,-133,26,-37,-79,-87,-18,-130,31,-32,-73,-81,0,4,-2,-17,8,-27\r\n"\
    "10380,,13095,\"Dougherty County, GA\",County or equivalent,94565,94565,94564,94889,94587,93225,92515,91332,-1,325,-302,-1362,-710,-1183,380,1570,1386,1403,1283,1342,267,761,893,922,908,886,113,809,493,481,375,456,15,79,111,100,105,115,-130,-637,-916,-1904,-1152,-1739,-115,-558,-805,-1804,-1047,-1624,1,74,10,-39,-38,-15\r\n"\
    "10380,,13177,\"Lee County, GA\",County or equivalent,28298,28298,28424,28604,28719,29046,29159,29202,126,180,115,327,113,43,73,363,376,366,366,345,69,149,170,199,218,205,4,214,206,167,148,140,3,11,12,16,17,17,116,-45,-109,132,-69,-122,119,-34,-97,148,-52,-105,3,0,6,12,17,8\r\n"\
    "10380,,13273,\"Terrell County, GA\",County or equivalent,9315,9507,9517,9384,9233,9217,9120,9113,10,-133,-151,-16,-97,-7,29,128,138,142,107,127,7,102,99,107,94,97,22,26,39,35,13,30,0,1,1,1,1,1,-10,-162,-195,-49,-106,-44,-10,-161,-194,-48,-105,-43,-2,2,4,-3,-5,6\r\n"\
    "10380,,13321,\"Worth County, GA\",County or equivalent,21679,21679,21725,21611,21464,21052,20952,20699,46,-114,-147,-412,-100,-253,57,240,246,271,265,237,65,203,217,205,227,219,-8,37,29,66,38,18,0,-2,0,0,0,0,56,-154,-178,-480,-122,-267,56,-156,-178,-480,-122,-267,-2,5,2,2,-16,-4"
  end

  let!(:location) do
    create(:location,
           zip_code: '00605',
           cbsa: '10380',
           msa: 'Albany, GA',
           pop_2015: 150_000,
           pop_2014: 140_000)
  end

  let(:zip_to_cbsa_response) do
    double(success?: true)
  end

  let(:cbsa_to_msa_response) do
    double(success?: true)
  end

  before do
    expect(Faraday).to receive(:get).with(ENV.fetch('ZIP_TO_CBSA_CSV_URL'))
      .and_return(zip_to_cbsa_response)
    expect(zip_to_cbsa_response).to receive_message_chain(:body, :force_encoding)
      .and_return(zip_to_cbsa_data)
    expect(Faraday).to receive(:get).with(ENV.fetch('CBSA_TO_MSA_CSV_URL'))
      .and_return(cbsa_to_msa_response)
    expect(cbsa_to_msa_response).to receive(:body)
      .and_return(cbsa_to_msa_data)
  end

  describe '#import' do
    it 'should create seven more locations' do
      expect { importer.import }.to change { Location.count }.from(1).to(8)
    end

    it 'should update existing location' do
      expect { importer.import }.to change { location.reload.pop_2015 }.from(150_000).to(153_526)
        .and change { location.reload.pop_2014 }.from(140_000).to(155_020)
    end

    it 'should leave cbsa blank for locations without cbsa' do
      importer.import
      expect(Location.find_by(zip_code: '00606').cbsa).to be_nil
      expect(Location.find_by(zip_code: '00610').cbsa).to be_nil
    end

    it 'should save correct cbsa for location that had updated cbsa' do
      importer.import
      expect(Location.find_by(zip_code: '00601').cbsa).to eq('10420')
    end

    it 'should save correct msa for all locations' do
      importer.import
      expect(Location.where(zip_code: %w[
        00501
        00601
        00602
        00603
        00604
        00605
        00606
        00610
      ]).pluck(:msa)).to match_array([
        'Abilene, TX',
        'Akron, OH',
        'Albany, GA',
        'Albany, GA',
        'Albany, GA',
        'Albany, GA',
        nil,
        nil
      ])
    end
  end
end
